require('dotenv').config();
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config');
const mongoose = require('mongoose');

var index_routes = require('./routes/index');
var users_routes = require('./routes/user');
var movie_list_routes = require('./routes/movie-list');

const app = express();

mongoose.connect(config.dataBase, { useNewUrlParser: true, useUnifiedTopology: true });
app.set('tokenSecret', config.secret);
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('database open');
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if (req.method === 'OPTIONS') {
    res.header("Access-Control-Allow-Methods", "PUT,POST,DELETE");
    return res.status(200).json({});
  }
  next();
});

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api', index_routes);
app.use('/api/user', users_routes);
app.use('/api/movie-list', movie_list_routes);

app.use(express.static(path.join(__dirname, 'theviewcue-frontend/build')));
app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, './theviewcue-frontend/build', 'index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    return res.json({
      status: "error",
      data: {
        message: err.message,
      }
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  return res.json({
    status: "error",
    data: {
      message: err.message,
    }
  });
});


module.exports = app;
