var mongoose = require('mongoose');
var movieListSchema = require('./movieList');

var userSchema = new mongoose.Schema({
    username: String,
    email: { type: String, unique: true, required: true, trim: true},
    password: { type: String, required: true},
    createdAt: {type: Date, default: new Date},
    updatedAt: {type: Date, default: new Date},
    movieList: [movieListSchema]
});

var User = mongoose.model('User', userSchema);

module.exports = User;