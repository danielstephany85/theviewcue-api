var mongoose = require('mongoose');

var movieListSchema = new mongoose.Schema({
    title: String,
    movieId: String,
    poster_path: String,
    createdAt: { type: Date, default: new Date },
    updatedAt: { type: Date, default: new Date },
});


movieListSchema.method('update', function (updates, cb) {
    Object.assign(this, updates, { updatedAt: new Date });
    this.parent().save(cb);
});

module.exports = movieListSchema;