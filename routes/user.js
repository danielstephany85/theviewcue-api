var express = require('express');
var router = express.Router();
var User = require('../models/user');
var middleware = require('../middleware/index');
const bcrypt = require('bcrypt');

router.param('userId', (req, res, next, id) => {
  User.findOne({ _id: id }, { password: false })
    .exec((err, doc) => {
      if (err) {
        return res.status(403).json({
          success: false,
          message: err.message
        });
      }
      if (!doc) {
        res.status(404).json({
          success: false,
          message: 'User not found'
        });
      }
      req.user = doc;
      return next();
    });
});

/* GET users listing. */
router.get('/:userId', middleware.validateToken, (req, res, next) => {
  if(req.user){
    return res.status(200).json({
      status: "success",
      data: {
        user: req.user,
      }
    });
  }else{
    return res.status(403).json({
      status: "fail",
      data: {
        message: "user not found",
      }
    });
  }
});


router.put('/:userId', middleware.validateToken, (req, res, next)=>{
  if(req.user){
    if (req.body.username) { req.user.username = req.body.username}
    if(req.body.email){req.user.email = req.body.email}
    if(req.body.password){
      bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) return  res.status(403).json({status: "fail",data: {message: "user not found"} });
        req.user.password = hash;
        updateUser(req.user);
        });
    }else {
      updateUser(req.user);
    }
  }else{
    return res.status(403).json({
      status: "fail",
      data: {
        message: "user not found",
      }
    });
  }

  function updateUser(user) {
    user.update(user, (err, result) => {
      if (err) {
        return res.status(403).json({
          status: "fail",
          data: {
            message: "username, email and password is required",
          }
        });
      }
      return res.json({
        success: true,
        data: null
      });
    });
  }
});

router.delete('/:userId', middleware.validateToken, (req, res, next)=>{
  if(req.user){
    req.user.remove((err)=>{
      if (err) { return res.status(403).json({ status: "fail", data: { message: "user not found" } });}
      return res.json({
        success: true,
        data: null
      });
    });
  }
});

module.exports = router;
