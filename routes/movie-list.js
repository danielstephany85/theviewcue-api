var express = require('express');
var router = express.Router();
var User = require('../models/user');
var middleware = require('../middleware/index');

router.param('userId', (req, res, next, id)=>{
    User.findById(id)
    .exec((err, doc)=>{
        if(err){ return res.status(403).json({status: "fail", data: { message: err.message }}); }
        if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
        req.user = doc;
        return next();
    });
});

router.get('/:userId', middleware.validateToken, (req, res, next)=>{
    res.json({
        status: "success",
        data: {
            movieList: req.user.movieList
        }
    });
});

router.post('/:userId', middleware.validateToken, (req, res, next)=>{
    if (req.body.title, req.body.movieId, req.body.poster_path){
        var movieData = {
            title: req.body.title,
            movieId: req.body.movieId,
            poster_path: req.body.poster_path
        }
        req.user.movieList.push(movieData);
        req.user.save((err, user) => {
            if (err) return next(err);
            return res.json({
                status: "success",
                data: { movieList: user.movieList }
            });
        });
    }else {
        res.status(403).json({
            status: "fail",
            data: { message: "title, movieId and movieImgUrl are required" }
        });
    }
    
});

router.delete('/:userId/:movieId', middleware.validateToken, (req, res, next) => {
    console.log("test");
    if (req.user) {
        let movieListItem = req.user.movieList.id(req.params.movieId);
        console.log(movieListItem);
        movieListItem.remove((err) => {
            if (err) { return res.status(403).json({ status: "error", data: { message: "movie not found" } }); }
        });
        req.user.save((err) => {
            if (err) {
                return res.status(403).send({
                    status: "error",
                    message: err.message
                });
            }
            return res.status(200).send({
                status: "success",
                data: null
            });
        });
    }
});


module.exports = router;