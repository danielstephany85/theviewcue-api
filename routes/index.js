var express = require('express');
var router = express.Router();
var User = require('../models/user');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');

/* GET home page. */
router.post('/create-account', function(req, res, next) {
  if (req.body.username, req.body.email, req.body.password){
    User.findOne({ email: req.body.email })
      .exec(function (err, user) {
        if (err) {
          return next(err);
        } else if (user) {
          return res.json({
            status: "error",
            data:{
              message: `user with email: ${req.body.email} already exists.`,
              }
          });
        } else {
          var newUser = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
          }
          bcrypt.hash(newUser.password, 10, (err, hash) => {
            newUser.password = hash;
            User.create(newUser, function (err, user) {
              if (err) {
                return res.json({
                  status: "error",
                  data: {
                    message: err.message
                  }
                });
              }
              const payload = { id: user._id };
              var token = jwt.sign(payload, config.secret);
              //remove password from returned data
              delete user._doc.password;
              delete user._doc.__v;
              return res.json({
                status: "success",
                data: {
                  user: user,
                  token: token
                }
              });
            });
          });
        }
      });
  }else{
    return res.status(403).json({
      status: "error",
      data: {
        message: "username, email and password is required",
      }
    });
  }  
});

router.post("/authenticate", (req, res, next)=>{
  if(req.body.email && req.body.password){
    User.findOne({ email: req.body.email })
    .exec((err, user)=>{
      if(err) return next(err)
      if(!user){
        return res.json({
          status: "error",
          data: {
            message: "user email was not found"
          }
        });
      }else{
        bcrypt.compare(req.body.password, user.password, function (err, result) {
          if (result){
            const payload = { id: user._id };
            var token = jwt.sign(payload, config.secret);
            //remove password from returned data
            delete user._doc.password;
            delete user._doc.__v;
            return res.json({
              status: "success",
              data: {
                user: user,
                token: token
              }
            });
          }else{
            return res.json({
              status: "error",
              data: {
                message: "incorrect password"
              }
            });
          }
        });
      }
    })
  }else{
    return res.status(403).json({
      status: "fail",
      data: {
        message: "email and password is required",
      }
    });
  }
});

module.exports = router;
