var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config');

var validateToken = function(req, res, next){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, data: {message: 'Failed to authenticate token.' }});
            } else {
                req.decoded = decoded;
                if (req.decoded.id == req.user._id) {
                    return next();// token is valid
                } else {
                    return res.status(403).json({ success: false, data: { message: 'permission denied' } });
                }
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No token provided.'
        });
    }
}

module.exports.validateToken = validateToken;