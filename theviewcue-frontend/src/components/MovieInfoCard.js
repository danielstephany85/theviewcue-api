import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from "../actions/index";

class MovieInfoCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            movieInList: false,
            activeMovie: "test",
            movieListItemId: "test",
            pendingListItem: false
        }
    }

    setActiveMovieData = bindActionCreators(Actions.setActiveMovieData, this.props.dispatch);
    addToList = bindActionCreators(Actions.addToList, this.props.dispatch);
    removeFromList = bindActionCreators(Actions.removeFromList, this.props.dispatch);

    componentDidMount = () => {
        this.checkIfInList();
    }

    componentDidUpdate = () => {
        if (this.state.activeMovie !== this.props.movieData.id){
            this.checkIfInList();
        }
    }
    componentWillUnmount = () => {
        this.setActiveMovieData(undefined);
    }

    checkIfInList = () => {
        if(!this.props.user) return;
        if(!this.props.user.movieList.length) return;
        let inList = false;
        this.props.user.movieList.forEach((listItem)=>{
            if (String(this.props.movieData.id) === String(listItem.movieId)){
                this.setState({
                    movieInList: true,
                    activeMovie: this.props.movieData.id,
                    movieListItemId: listItem._id,
                });
                inList = true;
            }
        });
        if(!inList){
            this.setState({ 
                movieInList: false,
                activeMovie: this.props.movieData.id 
            });
        }
    }

    HandleListActions = () => {
        let listItemData = {
            title: this.props.movieData.title,
            movieId: this.props.movieData.id,
            poster_path: this.props.movieData.poster_path,
        }
        if(!this.state.movieInList){
            this.setState({pendingListItem: true}, ()=>{
                    this.addToList(listItemData,this.props.user._id, this.props.token).then((res) => {
                        if (res.status === "success") {
                            let movieListItem = [];
                            if (res.data.movieList.length) {
                                movieListItem = res.data.movieList.filter((item) => String(item.movieId) === String(this.props.movieData.id));
                            }
                            this.setState({
                                movieInList: !this.state.movieInList,
                                movieListItemId: movieListItem[0]._id,
                                pendingListItem: false
                            }, () => { this.checkIfInList()});
                        }
                    });
                }
            );
        }else {
            this.setState({ pendingListItem: true },()=>{
                this.removeFromList(this.state.movieListItemId, this.props.user._id, this.props.token)
                    .then((data) => {
                        if (data.status === "success"){
                            this.setState({
                                movieInList: !this.state.movieInList,
                                pendingListItem: false
                            }, () => { this.checkIfInList() });
                        } 
                    });
                }
            );
            
        }
    }

    render = () => {
        if (!this.props.movieData) return "loading";
        const movieImg = this.props.movieData.poster_path;
        const overview = this.props.movieData.overview;
        const title = this.props.movieData.title;
        const activeClass = this.state.movieInList ? 'active' : '';
        const pendingClass = this.state.pendingListItem ? 'pending' : '';

        const listBtn =
            <button type="button" className={`toggle-button ${activeClass} ${pendingClass} ${this.props.signedIn? '':'deactivated'}`} onClick={this.HandleListActions}>
            {this.state.movieInList ? <span>remove from list</span> : <span>add to list</span>}
            <span className="icon toggles">
                <i className="fas fa-plus unchecked"></i>
                <i className="fas fa-check checked"></i>
                <i className="fas fa-spinner loading"></i>
            </span>
        </button>;

        return(
            <div className="movie-info-card">
                <div className="movie-info-card__left">
                    <div className="poster-container">
                        <div className="poster" style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w300_and_h450_bestv2/${movieImg})` }}>
                        </div>
                    </div>
                </div>
                <div className="movie-info-card__right">
                    <h2 className="movie-card-title">{title}</h2>
                    <div className="movie-card-actions">
                        <div className="poster-container">
                            <div className="poster" style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w300_and_h450_bestv2/${movieImg})` }}></div>
                        </div>
                        <ul className="action-list">
                            <li>
                                {this.props.signedIn ? listBtn:''}
                            </li>
                            {this.props.signedIn ? '' : <li className="signup-prompt"><NavLink to="/sign-up">Signup</NavLink> or <NavLink to="/log-in">Login</NavLink> to add to your list</li>  }
                        </ul>
                    </div>
                    <div className="movie-card-description">
                        <h3>Overview</h3>
                        <p>{overview}</p>
                    </div>
                </div>
            </div>
        );
    }
}

MovieInfoCard.propTypes = {
    movieData: PropTypes.object,
    user: PropTypes.object,
    signedIn: PropTypes.bool.isRequired,
}

const mapStateToProps = state => (
    {
        movieData: state.activeMovieData,
        signedIn: state.signedIn,
        user: state.user,
        genres: state.genres,
        token: state.token
    }
);

export default connect(mapStateToProps)(MovieInfoCard);