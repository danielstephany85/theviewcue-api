import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Poster extends Component {
    
    render = () =>{
        const posterPath = this.props.movieData.poster_path
        if (!posterPath) { return null;}
        return(
            <div className={this.props.listView? "poster list-view":"poster"} style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w300_and_h450_bestv2${posterPath})` }} >
                <div className="poster__overlay">
                    <h3>{this.props.movieData.original_title}</h3>
                </div>
            </div>
        );
    }
}

Poster.propTypes = {
    movieData: PropTypes.object.isRequired
}

export default Poster;