import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
// import GenreSelector from './GenreSelector';
import SearchOverlay from './SearchOverlay';
import UserInfoPopup from './UserInfoPopup';
import logo from "../imgs/theviewcue.svg";

class Header extends Component {
    state = {
        searchActive: false
    }

    toggleSearch = () => {
        this.setState({searchActive: !this.state.searchActive});
    }

    render = () =>{
        return (
            <div className="header-wrappr">
                <header className="main-header">
                    <NavLink className="main-header__logo" to="/">
                        <img src={logo} alt="the view cue" />
                    </NavLink>
                    <nav className="main-nav">
                        {!this.props.signedIn ? <NavLink className="main-nav__link main-btn" to='/sign-up'>Sign Up</NavLink> : ""}
                        {!this.props.signedIn ? <NavLink className="main-nav__link main-btn" to='/log-in'>Log In</NavLink> : ""}
                        <button type="button" className="icon-item" onClick={this.toggleSearch}><i className="fas fa-search"></i></button>
                        {this.props.signedIn ? <UserInfoPopup history={this.props.history} logOut={this.props.logOut}/> : ""}
                    </nav>
                </header>
                {/* {this.props.genres.length ? <GenreSelector genres={this.props.genres} /> : undefined} */}
                {this.state.searchActive ? <SearchOverlay toggleSearch={this.toggleSearch} history={this.props.history} />:undefined}
            </div>
        );
    }
}

Header.propTypes = {
    signedIn: PropTypes.bool.isRequired,
    logOut: PropTypes.func.isRequired
}

export default Header;