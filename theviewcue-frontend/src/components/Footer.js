import React from 'react';

const Footer = ()=>{
    return(
        <footer className="main-footer">
            <span className="main-footer__credit"><a href="http://danielstephany.com/" rel="noopener noreferrer" target="_blank">Created by: Daniel Stephany</a></span>
        </footer>
    );
}

export default Footer;