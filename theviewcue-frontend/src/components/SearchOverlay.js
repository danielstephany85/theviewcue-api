import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import config from '../config';

class SearchOverlay extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            searchActive: this.props.searchActive,
            searchInput: "",
            searchData: {
                results: undefined,
                page: undefined,
                total_pages: undefined
            }
        }
    }
    bounceSearch = true;
    lastQuery = false;

    componentDidMount = () => {
        this.searchInput.focus();
    }

    searchOnChange = (e) => {
        this.setState({
            searchInput: e.target.value
        },()=>{
            if (this.bounceSearch) {
                this.bounceSearch = false;
                setTimeout(() => {
                    if (this.state.searchInput !== this.lastQuery){
                        this.lastQuery = this.state.searchInput;
                        this.handleFetch(this.state.searchInput);
                    }
                    this.bounceSearch = true
                }, 400);
            }
        });
    }

    handleFetch = (query) => {
        fetch(`https://api.themoviedb.org/3/search/movie?api_key=${config.apiKey}&language=en-US&query=${query}&page=1&include_adult=false`)
        .then((res)=>{
            if(res){
                const json = res.json();
                return json
            }
        }).then((data)=>{
            const searchData = {
                results: data.results,
                page: data.page,
                total_pages: data.total_pages
            }
            this.setState({ 
                searchData: {
                    ...searchData
                }
            });
        });
    }

    closeSearchOverlay = (e) => {
        if (e.target.classList.contains('search-overlay')){
            this.props.toggleSearch();
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.history.push(`/search-list-view/${this.state.searchInput}`);
        this.props.toggleSearch();
    }

    render = () => {
        let quickLinks;
        let showLinks;
            if (this.state.searchData.results) {
                if (this.state.searchData.results.length){
                    quickLinks = this.state.searchData.results.filter((item, i) => {
                        if (i < 7) return true;
                        return false;
                    }).map((item, i) => {
                        return <NavLink key={i} to={`/movie-card-view/${item.id}`} onClick={this.props.toggleSearch}>{item.title}</NavLink>
                    });
                    showLinks = true;
                }else {
                    showLinks = false;
                }
            }
       

        return(
            <div className="search-overlay" onClick={(e)=>{this.closeSearchOverlay(e)}}>
                <div className="search-overlay__content">
                    <div className="search-card">
                        <div className="search-form">
                            <form onSubmit={(e)=>{this.handleSubmit(e)}}>
                                <button type='button' className="back-btn" onClick={this.props.toggleSearch}><i className="fas fa-arrow-left"></i><i className="fas fa-times"></i></button>
                                <input type="text" placeholder="search" value={this.state.searchInput} ref={(input)=>{this.searchInput = input}} onChange={(e)=>{this.searchOnChange(e)}}/>
                                <button className="search-btn" type='submit'><i className="fas fa-search"></i></button>
                            </form>
                        </div>
                        {
                            showLinks?
                                <div className="search-results-list">
                                    {quickLinks}
                                </div>
                                :
                                <span className="search-info" >Below quick links will be displayed to take you directly to a specific search item. To go to a list of search resulsts hit enter or press the search button. </span>    
                        }
                    </div>
                </div>
            </div>
        );
    }
}

SearchOverlay.propTypes = {
    toggleSearch: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired
}

export default SearchOverlay;