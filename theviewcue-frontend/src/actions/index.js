import config from '../config';

export const FETCHING_DATA = "FETCHING_DATA";
function fetchingData() {
    return {
        type: FETCHING_DATA
    }
}

export const SET_GENRES = "SET_GENRES"
function setGenres (data){
    return {
        type: SET_GENRES,
        genres: data
    }
}

export const SET_GENRE_MOVIE_LISTS = "SET_GENRE_MOVIE_LISTS"
function setGenreMovieLists(data) {
    return {
        type: SET_GENRE_MOVIE_LISTS,
        genreMovieLists: data
    }
}

export const SET_USER = "SET_USER";
export function setUser (user, token){
    return {
        type: SET_USER,
        user: user,
        token: token
    }
}

export const LOG_OUT = "LOG_OUT";
export function logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    return {
        type: LOG_OUT
    }
}

export const ADD_TO_LIST = 'ADD_TO_LIST';
export function _addToList(movieList) {
    return {
        type: ADD_TO_LIST,
        movieList: movieList
    }
}

export const REMOVE_FROM_LIST = 'REMOVE_FROM_LIST';
export function _removeFromList(id) {
    return {
        type: REMOVE_FROM_LIST,
        id: id
    }
}

export const SET_ACTIVE_MOVIE = "SET_ACTIVE_MOVIE";
export function setActiveMovieData(data){
    return {
        type: SET_ACTIVE_MOVIE,
        activeMovieData: data
    }
}

export function addToList(listData, userId, token) {
    return dispatch => {
        return new Promise((resolve) => {
            dispatch(fetchingData());
            fetch(`${config.url}/api/movie-list/${userId}?token=${token}`, {
                body: JSON.stringify(listData), // must match 'Content-Type' header
                headers: { 'content-type': 'application/json' },
                method: 'POST',
                mode: 'cors', // no-cors, cors, *same-origin
            })
                .then((res) => {
                    let jsonRes = res.json();
                    return jsonRes;
                })
                .then((json) => {
                    dispatch(_addToList(json.data.movieList));
                    resolve(json);
                });
        });
    }
}

export function removeFromList(listItemId, userId, token) {
    return dispatch => {
        return new Promise((resolve) => {
            dispatch(fetchingData());
            fetch(`${config.url}/api/movie-list/${userId}/${listItemId}?token=${token}`, {
                headers: { 'content-type': 'application/json' },
                method: 'DELETE',
                mode: 'cors', // no-cors, cors, *same-origin
            })
                .then((res) => {
                    let jsonRes = res.json();
                    return jsonRes;
                })
                .then((json) => {
                    if(json.status === "success"){
                        dispatch(_removeFromList(listItemId));
                        resolve(json);
                    }
                });
        });
    }
}

export function getGenre(key, cb) {
    return dispatch => {
        return new Promise((resolve)=>{
            dispatch(fetchingData());
            fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${key}&language=en-US`)
                .then((res) => {
                    let jsonRes = res.json();
                    return jsonRes;
                })
                .then((json) => {
                    let data = json.genres;
                    dispatch(setGenres(data));
                    resolve(data);
                });
        });
    }
}

export function getGenreMovieList(key, genres) {
    return (dispatch) => {
        dispatch(fetchingData());
        let getGenresIndex = 0;
        let newGenreList = [];
        const getmovieset = () => {
            if (getGenresIndex === (genres.length)) {
                getGenresIndex = 0;
                return;
            }
            fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${key}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${genres[getGenresIndex].id}`)
                .then((res) => {
                    let jsonRes = res.json();
                    return jsonRes;
                })
                .then((json) => {
                    const filteredJson = json.results.filter((item) => {
                        if (item.poster_path) return true;
                        return false;
                    });
                    let data = {
                        genre: genres[getGenresIndex].name,
                        genreId: genres[getGenresIndex].id,
                        list: filteredJson
                    }
                    getGenresIndex++;
                    newGenreList.push(data);
                    // had to pass an new array to make state update
                    dispatch(setGenreMovieLists([...newGenreList]));
                    return getmovieset();
                });
        }
        getmovieset();
    }
}