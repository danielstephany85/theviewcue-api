import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const Pagination = (props) => {
    const total_pages = (props.total_pages <= 1000) ? props.total_pages:1000;
    const pageIndex = parseInt(props.page, 10);
    let indicatorIndex = pageIndex;
    if (indicatorIndex === (total_pages - 1)) {
        indicatorIndex = indicatorIndex - 1;
    }
    if (indicatorIndex === total_pages){
        indicatorIndex = indicatorIndex - 2;
    }

    return(
        <div className="pagination">
            {(pageIndex !== 1) ? <NavLink to={`/user-list-view?page=${(pageIndex - 1)}`} className="pagination_button pagination_button-back"><i className="fas fa-arrow-left"></i></NavLink> : undefined}
            {(pageIndex > 3) ? <NavLink to={`/user-list-view?page=1`} className="pagination_button">1...</NavLink> : undefined}
            {(pageIndex > 3) ? <NavLink to={`/user-list-view?page=${(pageIndex - 1)}`} className="pagination_button rm-screen-sm">{pageIndex - 1}</NavLink> : undefined}
            {(indicatorIndex !== 0) ? <NavLink to={`/user-list-view?page=${indicatorIndex}`} className={`pagination_button ${(indicatorIndex === pageIndex) ? "active" : ""}`}>{indicatorIndex}</NavLink> : undefined}
            <NavLink to={`/user-list-view?page=${indicatorIndex + 1}`} className={`pagination_button ${((indicatorIndex + 1) === pageIndex) ? "active" : ""}`}>{indicatorIndex + 1}</NavLink>
            <NavLink to={`/user-list-view?page=${indicatorIndex + 2}`} className={`pagination_button ${((indicatorIndex + 2) === pageIndex) ? "active" : ""}`}>{indicatorIndex + 2}</NavLink>
            {(pageIndex !== total_pages) ? <NavLink to={`/user-list-view?page=${(pageIndex + 1)}`} className="pagination_button pagination_button-next"><i className="fas fa-arrow-right"></i></NavLink> : undefined}
        </div>
    );
}

Pagination.propTypes = {
    page: PropTypes.number.isRequired,
    total_pages: PropTypes.number.isRequired,
}

export default Pagination;