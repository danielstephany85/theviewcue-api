import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import MovieSlider from './MovieSlider/MovieSlider';
import MovieInfoCard from '../../components/MovieInfoCard';

class MovieGroup extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeMovieData: undefined,
            drawerOpen: false
        }
        this.html = document.querySelector('html');
        this.body = document.querySelector('body');
    }

    componentDidMount = () => {
        window.addEventListener('resize', this.handleResize, { useCapture: true });
    }
    componentWillUnmount = () => {
        window.removeEventListener('resize', this.handleResize, { useCapture: true });
    }

    handleResize = () => {
        if (this.state.drawerOpen && parseInt(this.drawer.style.height[0], 10)) {
            this.drawer.style.height = `${this.drawerContent.clientHeight}px`;
        }
    }

    setDrawerContent = (id, cb) => {
        const drawerMovieData = this.props.movieData.filter((movie)=>{
            if(movie.id === id) return true;
            return false;
        });
        this.props.setActiveMovieData(drawerMovieData[0]);
        this.setState({
            drawerOpen: true
        },()=>{
            if(typeof cb === 'function') return cb();
        });
    }

    toggleDrawer = (id) =>{
        let offset = 0;
        const drawer = this.drawer
        if (!drawer.clientHeight) {
            this.props.closeOpenDrawer();
            offset = 300;
        }
        this.props.getOpenDrawer(this.drawer);
        this.setDrawerContent(id, () => {
            drawer.style.height = `${this.drawerContent.clientHeight}px`;
            drawer.style.marginTop = "15px";
        });
        if ((window.scrollY + (window.innerHeight / 2)) < this.drawer.offsetTop){
            setTimeout(() => {
                window.scrollTo({
                    top: (this.movieGroup.offsetTop - 20),
                    left: 0,
                    behavior: 'smooth'
                });
            }, offset);
        }        
    }

    closeDrawer = () => {
        this.drawer.style.height = 0;
        this.drawer.style.marginTop = 0;
        this.setState({ drawerOpen: false });
    }

    render = () => {

        return(
            <div className="movie-group" ref={(div) => { this.movieGroup = div }}>
                <NavLink className="movie-type-link" to={`/movie-list-view/${this.props.movieGenreId}?genre=${this.props.movieGenre}`}>{this.props.movieGenre}</NavLink>
                <MovieSlider list={this.props.movieData} toggleDrawer={this.toggleDrawer}/>
                <div className='movie-group__drawer' ref={(div)=>{this.drawer = div }}>
                    <div className="drawer-content" ref={(div)=>{ this.drawerContent = div }}>
                        <button className="close-drawer" onClick={this.closeDrawer}><i className="fas fa-times"></i></button>
                            {this.props.activeMovieData? <MovieInfoCard/>:undefined}
                    </div>
                </div>
            </div>
        );
    }
}

MovieGroup.propTypes = {
    activeMovieData: PropTypes.object,
    movieData: PropTypes.array.isRequired,
    movieGenre: PropTypes.string.isRequired,
    getOpenDrawer: PropTypes.func.isRequired,
    closeOpenDrawer: PropTypes.func.isRequired,    
};

export default MovieGroup;