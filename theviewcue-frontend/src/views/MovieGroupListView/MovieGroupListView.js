import React, {Component} from 'react';
import MovieGroup from './MovieGroup';
import PropTypes from 'prop-types';
import GenreSelector from '../../components/GenreSelector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from "../../actions/index";

class MovieGroupListView extends Component {

    setActiveMovieData = bindActionCreators(Actions.setActiveMovieData, this.props.dispatch);

    getOpenDrawer = (openDrawer) => {
        this.openDrawer = openDrawer;
    }
    closeOpenDrawer = () => {
        if (this.openDrawer) {
            this.openDrawer.style.height = 0;
            this.openDrawer.style.marginTop = "15px";
        }
    }

    render = () => {
        let movieGroups 
        try {
            movieGroups = this.props.genreMovieLists.map((item, i) => {
                return <MovieGroup
                    key={i}
                    movieData={item.list}
                    movieGenre={item.genre}
                    movieGenreId={item.genreId}
                    getOpenDrawer={this.getOpenDrawer}
                    closeOpenDrawer={this.closeOpenDrawer}
                    activeMovieData={this.props.activeMovieData}
                    setActiveMovieData={this.setActiveMovieData}
                />
            });
        }catch(e){
            movieGroups = "loading";
        }

        return (
            <section>
                {this.props.genres.length ? <GenreSelector genres={this.props.genres} /> : undefined}
                {movieGroups}
            </section>
        );
    }
}

MovieGroupListView.propTypes = {
    genreMovieLists: PropTypes.array.isRequired,
    activeMovieData: PropTypes.object,
    genres: PropTypes.array.isRequired,
}

const mapStateToProps = state => (
    {
        genreMovieLists: state.genreMovieLists,
        activeMovieData: state.activeMovieData,
        genres: state.genres,
    }
);

export default connect(mapStateToProps)(MovieGroupListView);
