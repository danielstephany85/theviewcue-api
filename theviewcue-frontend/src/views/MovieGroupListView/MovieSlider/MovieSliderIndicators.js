import React from 'react';
import PropTypes from 'prop-types';

const MovieSliderIndicator = (props) =>{
    const indicators = [];
    // console.log(props.slidesLength);

    for (let i = 0; i < props.slidesLength; i++){
        indicators.push(<button key={i} type="button" className={(props.activeSlide === i) ? "active" : ""}></button>);
    }

    return (
        <div className="movie-slider__indicators">
            {indicators}
        </div>
    );
}

MovieSliderIndicator.propTypes = {
    slidesLength: PropTypes.number.isRequired,
    activeSlide: PropTypes.number.isRequired
}

export default MovieSliderIndicator;