import React, {Component} from 'react';
import PropTypes from "prop-types";
import Pagination from './Pagination';
import Poster from '../../components/Poster';
import { NavLink } from 'react-router-dom';
import GenreSelector from '../../components/GenreSelector';
import { connect } from 'react-redux';
import config from '../../config';

class MovieListView extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: undefined,
            lastGenreId: undefined
        }
        this.unmount = false;
    }
    componentWillMount = () => {
        this.searchParams = this.getSearchParams();
    }

    componentDidMount = () => {
        this.getMovieList(this.props.match.params.id, this.searchParams.page, this.searchParams.genre);
    }

    componentDidUpdate = () => {
        window.scrollTo(0, 0);
        this.searchParams = this.getSearchParams();
        if (!this.searchParams.page) this.searchParams.page = 1;
        if (this.state.lastGenreId !== undefined){
            if (this.state.lastGenreId !== this.props.match.params.id || parseInt(this.searchParams.page, 10) !== parseInt(this.state.page, 10)) {
                this.getMovieList(this.props.match.params.id, this.searchParams.page, this.searchParams.genre);
            }  
        }
    }

    getSearchParams = () => {
        let searchParams = {};
        var queryParams = this.props.location.search;
        if (this.props.location.search[0] === "?") queryParams = queryParams.slice(1);
        queryParams = queryParams.split("?");
        queryParams.forEach((query) => {
            let qp = query.split("=");
            qp[1] = qp[1].replace('%20',' ');
            searchParams[qp[0]] = qp[1];
        });
        return searchParams;
    }

    getMovieList = (genreId, page=1, genre) => {
        fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${config.apiKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}&with_genres=${genreId}`)
            .then((res) => {
                let jsonRes = res.json();
                return jsonRes;
            })
            .then((json) => {
                this.setState({ 
                    page: parseInt(page, 10),
                    total_pages: json.total_pages,
                    list: json.results,
                    lastGenreId: genreId,
                    genre: genre
                 });
            });
    }


    render = () => {
        let movies = undefined;
        if(this.state.list){
            let filteredList = this.state.list.filter((data) => data.backdrop_path);
            movies = filteredList.map((data, i)=>{
                return <NavLink to={`/movie-card-view/${data.id}`} key={i} className="movie-list__item"><Poster listView={true} movieData={data} /></NavLink>
            });
        }

        return(
            <section>
                {this.props.genres.length ? <GenreSelector genres={this.props.genres} /> : undefined}
                <div className="movie-list">
                    <h3>{this.state.genre}</h3>
                    <div className="movie-list__row">
                        {movies ? movies : undefined}
                    </div>
                    <div className="pagination-container">
                        {this.state.page ? <Pagination page={this.state.page} total_pages={this.state.total_pages} id={this.props.match.params.id} genre={this.searchParams.genre} /> : undefined}
                    </div>
                </div>
            </section>
        );
    }
}

MovieListView.propTypes = {
    genres: PropTypes.array.isRequired,
}

const mapStateToProps = state => (
    {
        genres: state.genres,
    }
);

export default connect(mapStateToProps)(MovieListView);
