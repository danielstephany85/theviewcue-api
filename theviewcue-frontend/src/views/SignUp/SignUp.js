import React, { Component } from 'react';
import config from '../../config';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {setUser} from "../../actions/index";

class SignUp extends Component {
    constructor(props){
        super(props);
        this.state = {
            userNameInput: '',
            emailInput: '',
            passwordInput: '',
            password2Input: '',
            userNameErr: '',
            emailErr: '',
            passwordErr: '',
            password2Err: '',
        }
    }

    setUser = bindActionCreators(setUser, this.props.dispatch);

    componentDidMount = () => {
        this.userNameInput.focus();
    }

    validateInputs = () => {
        let validBool = true;
        let errData = {};
        if (this.state.userNameInput === '') {
            errData.userNameErr = "user name is required";
            validBool = false;
        }
        if (this.state.emailInput === '') {
            errData.emailErr = "email is required";
            validBool = false;
        } else if (!this.state.emailInput.includes('@')) {
            errData.emailErr = "invalid email";
            validBool = false;
        }
        if(this.state.passwordInput === ''){
            errData.passwordErr = "password is required";
            validBool = false;
        }
        if (this.state.password2Input === '') {
            errData.password2Err = "re-enter password is required";
            validBool = false;
        }
        if (this.state.passwordInput !== this.state.password2Input && this.state.password2Input !== '' && this.state.passwordInput !== ''){
            errData.password2Err = "passwords do not match";
            validBool = false;
        }
        if (!validBool){
            this.setState({...errData});
        }
        return validBool
    }

    handleFetch = (payload) => {
        fetch(`${config.url}/api/create-account`, {
            body: JSON.stringify(payload), // must match 'Content-Type' header
            headers: {'content-type': 'application/json'},
            method: 'POST',
            mode: 'cors', // no-cors, cors, *same-origin
        }).then((res)=>{
            if(res){ return res.json(); }
        }).then((res)=>{
            if(res.status === 'success'){
                this.setUser(res.data.user, res.data.token)
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('userId', res.data.user._id);
                this.props.history.push('/');
            } else if (res.status === 'error' && res.data.message){
                this.setState({ emailErr: res.data.message });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            userNameErr: '',
            emailErr: '',
            passwordErr: '',
            password2Err: '',
        });
        const validBool = this.validateInputs();
        if (validBool){
            const payload = {
                username: this.state.userNameInput,
                email: this.state.emailInput,
                password: this.state.passwordInput
            }
            this.handleFetch(payload);
        }
    }

    render = () => {

        return (
            <section>
                <div className="content-card">
                    <div className="content-card__content">
                        <h2>Sign Up</h2>
                        <h3>Create an account to be able to create<br/> and save your own movie list.</h3>
                        <form className="main-form" onSubmit={(e)=>{this.handleSubmit(e)}}>
                            <div className="main-form__section">
                                <div className={this.state.userNameErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>User name</span>
                                        <input type="text" name="username" value={this.state.userNameInput} ref={(input)=>{this.userNameInput = input}} onChange={(e) => this.setState({ userNameInput: e.target.value })} />
                                    </label>
                                    {this.state.userNameErr ? <span className="error-message">{this.state.userNameErr}</span> : ''}
                                </div>
                                <div className={this.state.emailErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>Email</span>
                                        <input type="email" name="email" value={this.state.emailInput} onChange={(e)=>this.setState({emailInput: e.target.value})}/>
                                    </label>
                                    {this.state.emailErr ? <span className="error-message">{this.state.emailErr}</span> : ''}
                                </div>
                                <div className={this.state.passwordErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>Password</span>
                                        <input type="password" name="password" value={this.state.passwordInput} onChange={(e) => this.setState({ passwordInput: e.target.value })}/>
                                    </label>
                                    {this.state.password2Err ? <span className="error-message">{this.state.passwordErr}</span> : ''}
                                </div>
                                <div className={this.state.passwordErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>Re-enter Password</span>
                                        <input type="password" name="password2" value={this.state.password2Input} onChange={(e) => this.setState({ password2Input: e.target.value })}/>
                                    </label>
                                    {this.state.password2Err? <span className="error-message">{this.state.password2Err}</span> : '' }
                                </div>
                                <div className="main-form__item form-btn-container">
                                    <button className="form-button" type="submit">Sign Up</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}

export default connect()(SignUp);